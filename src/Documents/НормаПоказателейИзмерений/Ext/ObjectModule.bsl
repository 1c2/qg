﻿
Процедура ОбработкаПроведения(Отказ, Режим)

	// регистр НормыПоказателейИзмерений
	Движения.НормыПоказателейИзмерений.Записывать = Истина;
	Для Каждого ТекСтрокаПоказатели Из Показатели Цикл
		Движение = Движения.НормыПоказателейИзмерений.Добавить();
		Движение.Период = Дата;		
		Движение.Показатель = ТекСтрокаПоказатели.Показатель;
		Движение.Значение1 = ТекСтрокаПоказатели.Значение1;
		Движение.Значение2 = ТекСтрокаПоказатели.Значение2;
		Движение.ТипСравнения = ТекСтрокаПоказатели.ТипСравнения;
	КонецЦикла;

КонецПроцедуры
